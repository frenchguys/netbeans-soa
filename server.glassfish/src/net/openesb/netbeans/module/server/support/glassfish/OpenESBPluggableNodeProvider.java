package net.openesb.netbeans.module.server.support.glassfish;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.MBeanServerConnection;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import org.netbeans.modules.glassfish.common.GlassfishInstance;
import org.netbeans.modules.glassfish.common.GlassfishInstanceProvider;
import org.netbeans.modules.glassfish.spi.GlassfishModule;
import org.netbeans.modules.glassfish.spi.PluggableNodeProvider;
import org.netbeans.modules.sun.manager.jbi.nodes.api.NodeExtensionProvider;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class OpenESBPluggableNodeProvider implements PluggableNodeProvider {

    private static final Logger LOGGER = Logger.getLogger(OpenESBPluggableNodeProvider.class.getName());

    @Override
    public org.openide.nodes.Node getPluggableNode(Map<String, String> map) {
        if (map != null) {
            GlassfishInstance instance = GlassfishInstanceProvider.getGlassFishInstanceByUri(map.get(GlassfishModule.URL_ATTR));
            GlassfishModule.ServerState serverState = instance.getServerState();

            if (serverState == GlassfishModule.ServerState.RUNNING
                    || serverState == GlassfishModule.ServerState.RUNNING_JVM_DEBUG
                    || serverState == GlassfishModule.ServerState.RUNNING_JVM_PROFILER) {
                
                MBeanServerConnection mbsc = createMBeanServerConnection(instance);

                for (NodeExtensionProvider nep : Lookup.getDefault().lookupAll(NodeExtensionProvider.class)) {
                    if (nep != null) {
                        Node node = nep.getExtensionNode(mbsc);
                        if (node != null) {
                            return node;
                        }
                    }
                }
            }
        }

        return null;
    }

    private MBeanServerConnection createMBeanServerConnection(GlassfishInstance instance) {
        try {
            String username = instance.getAdminUser();
            String password = instance.getAdminPassword();
            
            Map<String, Object> env = new HashMap<String, Object>();
            
            if (username != null && !username.isEmpty()) {
                env.put(JMXConnector.CREDENTIALS, new String[]{username, password});
            }
            
            JMXConnector connector = JMXConnectorFactory.connect(new JMXServiceURL(
                    "service:jmx:rmi:///jndi/rmi://" + instance.getHost() + ":8686/jmxrmi"), env);

            return connector.getMBeanServerConnection();
        } catch (IOException ex) {
            LOGGER.log(Level.INFO, ex.getMessage(), ex);
            return null;
        }
    }

}
