package org.netbeans.test.xml.schema.abe;

/**
 *
 * @author Mikhail Matveev
 */
public class DVNamespaceOperator extends DVGenericNodeOperator {
    
    public DVNamespaceOperator(DesignViewOperator dop){
        super(dop,"");
    }

}
