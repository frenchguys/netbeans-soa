package org.openesb.netbeans.modules.appui.welcome;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Insets;
import javax.swing.JPanel;
import javax.swing.border.Border;

/**
 *
 * @author David BRASSELY
 */
class ContentsPanel extends JPanel implements Constants {
    
    public ContentsPanel() {
        initComponents();
    }
    
    
    private void initComponents() {
        setLayout(new GridLayout(2, 2, 0, 0));
        
        addLink( "Link1", true, false, false );
        addLink( "Link2", false, false, true );
     //   addLink( "Link3", true, false, false );
     //   addLink( "Link4", false, false, true );
    //    addLink( "Link5", true, false, false );
    //    addLink( "Link6", false, false, true );
        addLink( "Link3", true, true, false );
        addLink( "Link4", false, true, true );
        
        setBackground(Utils.getColor(COLOR_CONTENT_BACKGROUND));
    }
    
    private void addLink( String resourceKey, boolean includeSource, boolean drawBottom, boolean drawRight ) {
        JPanel panel = new JPanel(new BorderLayout());
        panel.setOpaque( false );
        panel.add( new WebLink(resourceKey, includeSource), BorderLayout.CENTER );
        panel.setBorder( new MyBorder(drawBottom, drawRight) );
        add( panel );
    }
    
    private static class MyBorder implements Border {
        private static final Color COLOR = Utils.getColor(BORDER_COLOR);
        private boolean drawBottom;
        private boolean drawRight;
        public MyBorder( boolean drawBottom, boolean drawRight ) {
            this.drawBottom = drawBottom;
            this.drawRight = drawRight;
        }
        
        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
            g.setColor(COLOR);
            g.drawLine(x, y, x+width, y);
            g.drawLine(x, y, x, y+height);
            if( drawRight ) 
                g.drawLine(x+width-1, y, x+width-1, y+height);
            if( drawBottom )
                g.drawLine(x, y+height-1, x+width, y+height-1);
        }

        public Insets getBorderInsets(Component c) {
            return new Insets(8,8,8,8);
        }

        public boolean isBorderOpaque() {
            return false;
        }
    }
}
